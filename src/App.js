import React, { useState } from 'react';
import _ from 'lodash';

import './App.css';
import calculateMortgagePayments from './calculator';

const App = () => {
  // state for principal amount
  const [principalAmount, setPrincipalAmount] = useState('100000');
  // state for interest rate
  const [interestRate, setInterestRate] = useState('5');
  // state for amortization period
  const [amortizationPeriod, setAmortizationPeriod] = useState(25);
  // state for payment frequency
  const [paymentFrequency, setPaymentFrequency] = useState('12');
  // state for payment term
  const [paymentTerm, setPaymentTerm] = useState(5);

  // amortization period in years
  const amortizationPeriodData = [
    { value: '1', name: '1 Year' },
    { value: '2', name: '2 Years' },
    { value: '3', name: '3 Years' },
    { value: '4', name: '4 Years' },
    { value: '5', name: '5 Years' },
    { value: '10', name: '10 Years' },
    { value: '15', name: '15 Years' },
    { value: '20', name: '20 Years' },
    { value: '25', name: '25 Years'}          
  ];

  // Payment term in years
  const paymentTermData = [
    { value: '1', name: '1 Year' },
    { value: '2', name: '2 Years' },
    { value: '3', name: '3 Years'},
    { value: '4', name: '4 Years'},
    { value: '5', name: '5 Years'},
    { value: '6', name: '6 Years'},
    { value: '7', name: '7 Years'},
    { value: '8', name: '8 Years'},
    { value: '9', name: '9 Years'},
    { value: '10', name: '10 Years'}          
  ];

  // Payment frequency in weekly, bi-weekly, bi-monthly, monthly
  const paymentFrequencyData = [
    { value: '52', name: 'Weekly' },
    { value: '26', name: 'Bi-Weekly' },
    { value: '24', name: 'Bi-Monthly'},
    { value: '12', name: 'Monthly'},   
  ];
  
  // Get calculated data
  const { 
    mortgagePayment,
    amotizationMonths,
    termInterest,
    amotizationTotalPayment, 
    termTotalPayment,
    amotizationInterest,
    termEndBalance,
    termPrincipalPayment
   } = calculateMortgagePayments(principalAmount, amortizationPeriod, interestRate, paymentFrequency, paymentTerm);

  return (
    <div className="container">
      <h2 className="main-header">Mortgage Calculator</h2>
        <form className="form-horizontal Mortgage-calculator-form">
          <div className="form-group">
            <label className="col-sm-2">Mortgage Amount</label>
            <div className="col-sm-10">
              <input className="form-control" name="principal-amount" maxLength={7} value={principalAmount} onChange={e => setPrincipalAmount(e.target.value)} />
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-2">Interest Rate</label>
            <div className="col-sm-10">          
              <input className="form-control" name="interest-rate" type="number" step={0.1} value={interestRate} onChange={e => setInterestRate(e.target.value)} />
            </div>
          </div>
          
          <div className="form-group">
            <label className="control-label col-sm-2">Amortization Period</label>
            <div className="col-sm-10">          
              <select className="form-control amortization-period" name="amortization-period" value={amortizationPeriod} onChange={e => setAmortizationPeriod(e.target.value)}>
                {amortizationPeriodData.map((e, key) => {
                  return <option key={key} value={e.value}>{e.name}</option>;
                })}
              </select>
            </div>
          </div>
          
          <div className="form-group">
            <label className="control-label col-sm-2">Payment Frequency</label>
            <div className="col-sm-10">          
              <select className="form-control payment-frequency" name="payment-frequency" value={paymentFrequency} onChange={e => setPaymentFrequency(e.target.value)}>
                {paymentFrequencyData.map((e, key) => {
                  return <option key={key} value={e.value}>{e.name}</option>;
                })}
              </select>
            </div>
          </div>
          
          <div className="form-group">
            <label className="control-label col-sm-2">Term</label>
            <div className="col-sm-10">          
            <select className="form-control payment-term" name="payment-term" value={paymentTerm} onChange={e => setPaymentTerm(e.target.value)}>
              {paymentTermData.map((e, key) => {
                return <option key={key} value={e.value}>{e.name}</option>;
              })}
            </select>
          </div>
        </div>
      </form>
      {amortizationPeriod >= paymentTerm ? // if payment term is more of amortization period - display error text
        (<div>
        <div className="col-sm-12">
          <h4 className="calculation-summary-header">Calculation Summary</h4>
          <table className="table table-hover table-bordered">
            <thead>
              <tr>
                <th><strong>Category</strong></th>
                <th><strong>Term</strong></th>
                <th><strong>Amotization Period</strong></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="summary-tbl-data">Number of Payments</td>
                <td className="summary-tbl-data">{paymentTerm * paymentFrequency}</td>
                <td className="summary-tbl-data">{amortizationPeriod * paymentFrequency}</td>
              </tr>
              <tr>
                <td className="summary-tbl-data">Mortgage Payment</td>
                <td className="summary-tbl-data">${mortgagePayment.toFixed(2)}</td>
                <td className="summary-tbl-data">${mortgagePayment.toFixed(2)}</td>
              </tr>
              <tr>
                <td className="summary-tbl-data">Prepayment</td>
                <td className="summary-tbl-data">$0</td>
                <td className="summary-tbl-data">$0</td>
              </tr>
              <tr>
                <td className="summary-tbl-data">Principal Payments</td>
                <td className="summary-tbl-data">${termPrincipalPayment}</td>
                <td className="summary-tbl-data">${principalAmount}</td>
              </tr>
              <tr>
                <td className="summary-tbl-data">Total Cost</td>
                <td className="summary-tbl-data">${termTotalPayment}</td>
                <td className="summary-tbl-data">${amotizationTotalPayment}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="col-sm-12">
          <h2 className="mortgage-summary-header">Mortgage Summary</h2>
          <h3 className="mortgage-payment">${(mortgagePayment).toFixed(2)}</h3>
          <div>
            <p>Over the {amortizationPeriod}-year amortization period, you will:</p>
            <ul>
              <li>have made <b>{amotizationMonths}</b> {_.find(paymentFrequencyData, ['value', paymentFrequency]).name} (12X per year) payments of <b>${(mortgagePayment).toFixed(2)}.</b></li>
              <li>have paid <b>${principalAmount}</b> in principal, <b>${amotizationInterest}</b> in interest, for a total of <b>${amotizationTotalPayment}</b></li>
            </ul>
          </div>
          <div>
            <p>Over the {paymentTerm}-year term, you will:</p>
            <ul>
              <li>have made <b>{paymentTerm * paymentFrequency}</b> {_.find(paymentFrequencyData, ['value', paymentFrequency]).name} payments of <b>{(mortgagePayment).toFixed(2)}.</b></li>
              <li>have paid <b>${termPrincipalPayment}</b> in principal, <b>${termInterest}</b> in interest, for a total of <b>${termTotalPayment}</b></li>
            </ul>
          </div>
          <div>
          <p>At the end of your {paymentTerm}-year term, you will:</p>
          <ul><li>have a balance of <b>${termEndBalance}</b></li></ul>
          </div>
        </div>
      </div>) 
      : <div id="error-msg">Amortization period must be greater or equal to term period.</div>
    }
    </div>
  );
};

export default App;