import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
Enzyme.configure({ adapter: new Adapter() });

describe('MortgageCalculator', () => {

  let calculatorInstance;
    
  beforeEach(() => {
    calculatorInstance = shallow(<App />);
  });

  it('should show the header text', () => {
  const element = calculatorInstance.find('div .main-header');
  expect(element.text()).toBe('Mortgage Calculator');
 });

 it('should have class Mortgage Calculator Form', () => {
  expect(calculatorInstance.find('.Mortgage-calculator-form')).toHaveLength(1);
});

 it('should show the calculation summary', () => {
  const element = calculatorInstance.find('div .calculation-summary-header');
  expect(element.text()).toBe('Calculation Summary');
 });

 it('should show the mortgage summary', () => {
  const element = calculatorInstance.find('div .mortgage-summary-header');
  expect(element.text()).toBe('Mortgage Summary');
 });

it('should display correct mortgage payment', () => {
  calculatorInstance.type('input[name=principal-amount]', '100000');
  calculatorInstance.type('input[name=interest-rate]', '4');
  calculatorInstance.find('.amortization-period').simulate('change',{target: { value : 25}});
  calculatorInstance.find('.payment-frequency').simulate('change',{target: { value : '12'}});
  calculatorInstance.find('.payment-term').simulate('change',{target: { value : 5}});
  const element = calculatorInstance.find('.mortgage-payment');
  expect(element.text()).toBe('$584.59');
});

it('should display error message', () => {
  calculatorInstance.type('input[name=principal-amount]', '100000');
  calculatorInstance.type('input[name=interest-rate]', '5');
  calculatorInstance.find('.amortization-period').simulate('change',{target: { value : 1}});
  calculatorInstance.find('.payment-frequency').simulate('change',{target: { value : '12'}});
  calculatorInstance.find('.payment-term').simulate('change',{target: { value : 5}});
  const element = calculatorInstance.find('#error-msg');
  expect(element.text()).toBe('Amortization period must be greater or equal to term period.');
});

});