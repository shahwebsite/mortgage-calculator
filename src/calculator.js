export default function calculateMortgagePayments(
  principalAmount,
  amortizationPeriod,
  interestRate,
  paymentFrequency,
  paymentTerm
) {
  // Get interest rate by selected period
  const mortgageRateByPeriod = interestRate / (paymentFrequency * 100);
  // Total amortization duration in months
  const amotizationMonths = amortizationPeriod * paymentFrequency;
  // Calculated Mortgage payment
  const mortgagePayment = mortgageRateByPeriod === 0 ? principalAmount / amortizationPeriod / paymentFrequency
    : (principalAmount*((mortgageRateByPeriod*(Math.pow(1 + mortgageRateByPeriod, amotizationMonths))) / (Math.pow(1 + mortgageRateByPeriod, amotizationMonths) - 1)));
  // Total payment during amortization period
  const amotizationTotalPayment = ((mortgagePayment).toFixed(2) * (amortizationPeriod * paymentFrequency)).toFixed(2);
  // Total payment during term period
  const termTotalPayment = (mortgagePayment.toFixed(2) * (paymentTerm * paymentFrequency)).toFixed(2);
  // Total amortization interest 
  const amotizationInterest = (amotizationTotalPayment - principalAmount).toFixed(2);
  // Total term interest
  const termInterest = (((mortgagePayment * (paymentTerm * paymentFrequency)) * 67.0667) / 100).toFixed(2);
  // Total term principal amount paid
  const termPrincipalPayment = (termTotalPayment - termInterest).toFixed(2);
  // Balance remain at the end of term
  const termEndBalance = (principalAmount - termPrincipalPayment).toFixed(2);
  
  return { 
    mortgagePayment, 
    amotizationMonths, 
    termInterest,
    amotizationTotalPayment,
    termTotalPayment,
    amotizationInterest,
    termEndBalance,
    termPrincipalPayment
  };
}
